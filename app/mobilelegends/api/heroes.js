var Heroes = require("../Model/Heroes.js/index.js");

module.exports = function(app) {
  app.post("/mobilelegends/heroes/add", (req, res) => {
    console.log(req.body);
    let data = req.body;

    Heroes.findOne({ _id: data._id }, (err, docs) => {
      if (docs !== null) {
        docs.name = data.profile.name;
        docs.profile = data.profile;
        docs.stats = data.stats;
        docs.attack = data.attack;
        docs.super = data.super;
        docs.star = data.star;
        docs.tips = data.tips;
        docs.save();
        res.json({ code: 200 });
      } else {
        record();
      }
    });

    function record() {
      new Heroes({
        ...data,
        name: data.profile.name, //for slug
        timestamp: Date.now()
      }).save(function(err, docs) {
        res.json({ code: 200 });
      });
    }
  });

  app.get("/mobilelegends/heroes/all", (req, res) => {
    console.log("request");
    Heroes.find({}, function(error, data) {
      console.log(data);
      res.json(data);
    });
  });

  app.get("/mobilelegends/heroes/get/:slug", (req, res) => {
    console.log("request");
    Heroes.findOne({ slug: req.params.slug }).exec(function(error, data) {
      getEvents(events => {
        // data.events = events
        //   .filter(x => {
        //     if (x.guide.Heroes !== undefined) {
        //       let check = x.guide.Heroes.some(s => s[1] == data._id);
        //       console.log(check);
        //       return check;
        //     }
        //     return false;
        //   })
        //   .map(x => {
        //     return {
        //       profile: x.profile,
        //       slug: x.slug,
        //       _id: x._id
        //     };
        //   });
        res.json(data);
      });
    });
  });

  app.post("/mobilelegends/heroes/delete", (req, res) => {
    console.log(req.body);
    let data = req.body;
    Heroes.findOneAndDelete({ _id: data._id }, function(err, doc) {
      res.json({ code: 200 });
    });
  });
};
