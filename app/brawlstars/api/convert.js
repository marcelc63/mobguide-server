var Brawlers = require("../Model/Brawlers.js");
var BrawlersNew = require("../Model/BrawlersNew.js");

var Events = require("../Model/Events.js");
var EventsNew = require("../Model/EventsNew.js");

var MapsData = require("../Model/Maps.js");
var MapsNew = require("../Model/MapsNew.js");

module.exports = function(app) {
  app.get("/convert/maps", (req, res) => {
    console.log("request");
    MapsData.find({}, function(error, data) {
      data.forEach((x, i) => {
        console.log(x.name);

        new MapsNew({
          name: x.profile.name,
          image: x.profile.image,
          layout: x.profile.layout,
          events: x.profile.events,
          tips: x.tips.items,
          timestamp: Date.now()
        }).save(function(err, docs) {});
      });
      res.send("hi");
    });
  });
  app.get("/convert/events", (req, res) => {
    console.log("request");
    Events.find({}, function(error, data) {
      data.forEach((x, i) => {
        console.log(x.name);

        new EventsNew({
          name: x.profile.name,
          objective: x.profile.objective,
          avatar: x.profile.avatar,
          description: x.profile.description,
          guideBrawlers: x.guide.brawlers !== undefined ? x.guide.brawlers : [],
          tips: x.tips.items,
          timestamp: Date.now()
        }).save(function(err, docs) {});
      });
      res.send("hi");
    });
  });
  app.get("/convert/brawlers", (req, res) => {
    console.log("request");
    Brawlers.find({}, function(error, data) {
      data.forEach((x, i) => {
        console.log(x.name);

        new BrawlersNew({
          name: x.profile.name,
          rarity: x.profile.rarity,
          portrait: x.profile.portrait,
          avatar: x.profile.avatar,
          description: x.profile.description,
          statsMovement: x.stats.movement,
          statsHealth: x.stats.health,
          attackName: x.attack.name,
          attackEffect: x.attack.effect,
          attackDescription: x.attack.description,
          attackStats: x.attack.stats,
          attackDamage: x.attack.damage,
          superName: x.super.name,
          superEffect: x.super.effect,
          superDescription: x.super.description,
          superStats: x.super.stats,
          superDamage: x.super.damage,
          starName: x.super.name,
          starEffect: x.star.effect,
          starDescription: x.star.description,
          tips: x.tips.items,
          timestamp: Date.now()
        }).save(function(err, docs) {});
      });
      res.send("hi");
    });
  });
};
