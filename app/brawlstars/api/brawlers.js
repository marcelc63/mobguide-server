var Brawlers = require("../Model/Brawlers.js");
var Events = require("../Model/Events.js");

function getEvents(callback) {
  Events.find({}).exec(function(error, data) {
    callback(data);
  });
}

module.exports = function(app) {
  app.post("/brawlstars/brawlers/add", (req, res) => {
    console.log(req.body);
    let data = req.body;

    Brawlers.findOne({ _id: data._id }, (err, docs) => {
      if (docs !== null) {
        docs.name = data.profile.name;
        docs.profile = data.profile;
        docs.stats = data.stats;
        docs.attack = data.attack;
        docs.super = data.super;
        docs.star = data.star;
        docs.tips = data.tips;
        docs.save();
        res.json({ code: 200 });
      } else {
        record();
      }
    });

    function record() {
      new Brawlers({
        ...data,
        name: data.profile.name, //for slug
        timestamp: Date.now()
      }).save(function(err, docs) {
        res.json({ code: 200 });
      });
    }
  });

  app.get("/brawlstars/brawlers/all", (req, res) => {
    console.log("request");
    Brawlers.find({}, function(error, data) {
      console.log(data);
      res.json(data);
    });
  });

  app.get("/brawlstars/brawlers/get/:slug", (req, res) => {
    console.log("request");
    Brawlers.findOne({ slug: req.params.slug }).exec(function(error, data) {
      getEvents(events => {
        // data.events = events
        //   .filter(x => {
        //     if (x.guide.brawlers !== undefined) {
        //       let check = x.guide.brawlers.some(s => s[1] == data._id);
        //       console.log(check);
        //       return check;
        //     }
        //     return false;
        //   })
        //   .map(x => {
        //     return {
        //       profile: x.profile,
        //       slug: x.slug,
        //       _id: x._id
        //     };
        //   });
        res.json(data);
      });
    });
  });

  app.post("/brawlstars/brawlers/delete", (req, res) => {
    console.log(req.body);
    let data = req.body;
    Brawlers.findOneAndDelete({ _id: data._id }, function(err, doc) {
      res.json({ code: 200 });
    });
  });
};
