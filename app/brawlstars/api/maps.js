var Model = require("../Model/Maps.js");
var Events = require("../Model/Events.js");

module.exports = function(app) {
  app.post("/brawlstars/maps/add", (req, res) => {
    console.log(req.body);
    let data = req.body;

    Model.findOne({ _id: data._id }, (err, docs) => {
      console.log(docs);
      if (docs !== null) {
        docs.name = data.profile.name;
        docs.profile = data.profile;
        docs.tips = data.tips;
        docs.save();
        res.json({ code: 200 });
      } else {
        record();
      }
    });

    function record() {
      new Model({
        ...data,
        name: data.profile.name, //for slug
        timestamp: Date.now()
      }).save(function(err, docs) {
        res.json({ code: 200 });
      });
    }
  });

  app.get("/brawlstars/maps/all", (req, res) => {
    console.log("request");
    if (req.query.populate) {
      Model.find({})
        .populate({
          path: "profile.events",
          model: "bsEvent",
          select: "profile slug"
        })
        .exec(function(err, data) {
          if (err) return console.log(err);
          res.json(data);
        });
      return;
    }
    Model.find({}, function(error, data) {
      console.log(data);
      res.json(data);
    });
  });

  app.get("/brawlstars/maps/get/:slug", (req, res) => {
    console.log("request");
    Model.findOne({ slug: req.params.slug })
      .populate({
        path: "profile.events",
        model: "bsEvent",
        select: "profile slug"
      })
      .exec(function(err, data) {
        if (err) return console.log(err);
        res.json(data);
      });
  });

  app.post("/brawlstars/maps/delete", (req, res) => {
    console.log(req.body);
    let data = req.body;
    Model.findOneAndDelete({ _id: data._id }, function(err, doc) {
      res.json({ code: 200 });
    });
  });
};
