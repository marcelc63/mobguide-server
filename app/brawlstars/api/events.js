var Events = require("../Model/Events.js");
var Brawlers = require("../Model/Brawlers.js");

function getBrawlers(callback) {
  Brawlers.find({}).exec(function(error, data) {
    callback(data);
  });
}

module.exports = function(app) {
  app.post("/brawlstars/events/add", (req, res) => {
    console.log(req.body);
    let data = req.body;

    Events.findOne({ _id: data._id }, (err, docs) => {
      if (docs !== null) {
        docs.name = data.profile.name;
        docs.profile = data.profile;
        docs.guide = data.guide;
        docs.tips = data.tips;
        docs.save();
        res.json({ code: 200 });
      } else {
        record();
      }
    });

    function record() {
      new Events({
        ...data,
        name: data.profile.name, //for slug
        timestamp: Date.now()
      }).save(function(err, docs) {
        res.json({ code: 200 });
      });
    }
  });

  app.get("/brawlstars/events/all", (req, res) => {
    console.log("request");
    Events.find({}, function(error, data) {
      console.log(data);
      res.json(data);
    });
  });

  app.get("/brawlstars/events/get/:slug", (req, res) => {
    console.log("request");
    Events.findOne({ slug: req.params.slug }, function(error, data) {
      getBrawlers(brawlers => {
        // console.log(brawlers.length);
        if (data.guide.brawlers !== undefined) {
          data.guide.brawlers = data.guide.brawlers.map(x => {
            let getBrawler = brawlers.filter(s => {
              console.log(s._id, x[1]);
              return s._id == x[1];
            })[0];
            let temp = [x[0], getBrawler];
            return temp;
          });
        } else {
          data.guide.brawlers = [];
        }

        // console.log(data);
        res.json(data);
      });
    });
  });

  app.post("/brawlstars/events/delete", (req, res) => {
    console.log(req.body);
    let data = req.body;
    Events.findOneAndDelete({ _id: data._id }, function(err, doc) {
      res.json({ code: 200 });
    });
  });
};
