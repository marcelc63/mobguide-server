module.exports = function(app) {
  require("./api/brawlers.js")(app);
  require("./api/events.js")(app);
  require("./api/maps.js")(app);
  require("./api/convert.js")(app);
};
