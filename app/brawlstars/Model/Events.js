var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var MySchema = new Schema({
  name: String,
  profile: {
    name: String,
    objective: String,
    avatar: String,
    description: String
  },
  guide: {
    brawlers: Array
  },
  tips: {
    items: Array
  },
  timestamp: Number,
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now
  }
});

MySchema.plugin(URLSlugs("name", { update: true }));

module.exports = mongoose.model("bsEvent", MySchema);
