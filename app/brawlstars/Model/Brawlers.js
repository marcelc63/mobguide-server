var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var MySchema = new Schema({
  name: String,
  profile: {
    name: String,
    rarity: String,
    portrait: String,
    avatar: String,
    description: String
  },
  stats: {
    movement: Number,
    health: Array
  },
  attack: {
    name: String,
    effect: String,
    description: String,
    stats: Array,
    damage: Array
  },
  super: {
    name: String,
    effect: String,
    description: String,
    stats: Array,
    damage: Array
  },
  star: {
    name: String,
    effect: String,
    description: String
  },
  tips: {
    items: Array
  },
  timestamp: Number,
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now
  }
});

MySchema.plugin(URLSlugs("name", { update: true }));

module.exports = mongoose.model("bsBrawler", MySchema);
