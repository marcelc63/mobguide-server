var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var MySchema = new Schema({
  name: String,
  rarity: String,
  portrait: String,
  avatar: String,
  description: String,
  statsMovement: Number,
  statsHealth: Array,
  attackName: String,
  attackEffect: String,
  attackDescription: String,
  attackStats: Array,
  attackDamage: Array,
  superName: String,
  superEffect: String,
  superDescription: String,
  superStats: Array,
  superDamage: Array,
  starName: String,
  starEffect: String,
  starDescription: String,
  tips: Array,
  timestamp: Number,
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now
  }
});

MySchema.plugin(URLSlugs("name", { update: true }));

module.exports = mongoose.model("bsBrawlerNew", MySchema);
