//Dependencies
let multer = require("multer");
let storage = multer.memoryStorage();
let upload = multer({ storage: storage });
let axios = require("axios");
let credentials = require("../config.js");

module.exports = function(app) {
  app.get("/", (req, res) => {
    res.send("hi");
  });

  app.post("/upload", upload.single("avatar"), function(req, res) {
    let image = req.file.buffer.toString("base64");
    console.log('upload')
    axios({
      method: "post",
      url: "https://api.imgur.com/3/image",
      data: {
        image: image,
        album: "y6Y7amj",
        type: "base64"
      },
      headers: {
        Authorization: credentials.imgur.clientId,
        Authorization: credentials.imgur.brearer
      }
    })
      .then(packet => {
        let url = packet.data.data.link;
        console.log(url)
        res.send({ link: url });
      })
      .catch(error => console.log(error));
  });

  require("./brawlstars/index.js")(app);
};
