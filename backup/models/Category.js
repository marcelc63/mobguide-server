var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;
var URLSlugs = require("mongoose-url-slugs");

var CategorySchema = new Schema({
  name: String,
  game: { type: ObjectId, ref: "Game", required: true },
  description: String,
  thumbnail: String,
  layout: String,
  timestamp: Number,
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now
  }
});

CategorySchema.plugin(URLSlugs("name"));

module.exports = mongoose.model("Category", CategorySchema);
