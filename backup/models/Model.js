var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var ObjectId = mongoose.Schema.Types.ObjectId;

var ModelSchema = new Schema({
  category: String,
  parentId: { type: ObjectId, ref: 'Category', required: true }, 
  sections: Array,
  timestamp: Number,
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now
  }
});

module.exports = mongoose.model("Model", ModelSchema);
