var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var URLSlugs = require("mongoose-url-slugs");

var GameSchema = new Schema({
  name: String,
  description: String,
  thumbnail: String,
  timestamp: Number,
  datestamp: {
    type: Date,
    // `Date.now()` returns the current unix timestamp as a number
    default: Date.now
  }
});

GameSchema.plugin(URLSlugs("name"));

module.exports = mongoose.model("Game", GameSchema);
