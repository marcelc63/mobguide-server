var Game = require("../models/Game.js");
var Category = require("../models/Category.js");
var View = require("../models/View.js");
var Model = require("../models/Model.js");

var path = require("path");

module.exports = function(app) {
  app.post("/game", (req, res) => {
    console.log(req.body);
    let data = req.body;
    new Game({
      ...data,
      timestamp: Date.now()
    }).save();

    res.json({ code: 200 });
  });

  app.get("/game", (req, res) => {
    console.log("request");
    Game.find({}, function(error, data) {
      console.log(data);
      res.json(data);
    });
  });
  
  app.get("/game/:id", (req, res) => {
    console.log("request with id");
    console.log(req.params.id);
    Game.find({ _id: req.params.id }, function(error, data) {
      console.log(data);
      res.json(data);
    });
  });

  app.post("/game/delete", (req, res) => {
    console.log(req.body);
    let data = req.body;

    Game.findOneAndDelete({ _id: data.id }, function(err, doc) {});
    // View.findOneAndDelete({ parentId: data.id }, function(err, doc) {});
    // Model.deleteMany({ parentId: data.id }, function(err, doc) {});

    res.json({ code: 200 });
  });

  app.post("/category", (req, res) => {
    console.log(req.body);
    let data = req.body;
    let category = new Category({
      ...data,
      timestamp: Date.now()
    }).save();

    res.json({ code: 200 });
  });

  app.post("/category/delete", (req, res) => {
    console.log(req.body);
    let data = req.body;

    Category.findOneAndDelete({ _id: data.id }, function(err, doc) {});
    View.findOneAndDelete({ parentId: data.id }, function(err, doc) {});
    Model.deleteMany({ parentId: data.id }, function(err, doc) {});

    res.json({ code: 200 });
  });

  app.get("/category/all/:id", (req, res) => {
    console.log("request");
    Category.find({ game: req.params.id }, function(error, data) {
      console.log(data);
      res.json(data);
    });
  });
  app.get("/category/:id", (req, res) => {
    console.log("request with id");
    console.log(req.params.id);
    Category.find({ _id: req.params.id }, function(error, data) {
      console.log(data);
      res.json(data);
    });
  });

  app.post("/view", (req, res) => {
    console.log(req.body);
    let data = req.body;

    View.findOne({ parentId: data.parentId }, (err, docs) => {
      console.log("checkDocs", docs);
      if (docs !== null) {
        console.log("exist");
        docs.sections = data.sections;
        docs.save();
      } else {
        record();
      }
    });

    function record() {
      let view = new View({
        ...data,
        timestamp: Date.now()
      }).save();
    }

    res.json({ code: 200 });
  });

  app.get("/view/:id", (req, res) => {
    console.log("request view with id");
    console.log(req.params.id);
    View.find({ parentId: req.params.id }, function(error, data) {
      console.log(data);
      res.json(data);
    });
  });

  app.post("/model", (req, res) => {
    console.log(req.body);
    let data = req.body;

    Model.findOne({ _id: data._id }, (err, docs) => {
      if (docs !== null) {
        docs.sections = data.sections;
        docs.save();
      } else {
        record();
      }
    });

    function record() {
      let model = new Model({
        ...data,
        timestamp: Date.now()
      }).save();
    }

    res.json({ code: 200 });
  });

  app.get("/model/:id", (req, res) => {
    console.log("request model with id");
    console.log(req.params.id);
    Model.find({ parentId: req.params.id }, function(error, data) {
      console.log(data);
      res.json(data);
    });
  });

  app.get("/model/single/:id", (req, res) => {
    console.log("request model with id");
    console.log(req.params.id);
    Model.find({ _id: req.params.id }, function(error, data) {
      console.log(data);
      res.json(data);
    });
  });

  app.post("/model/delete", (req, res) => {
    console.log(req.body);
    let data = req.body;
    Model.findOneAndDelete({ _id: data.id }, function(err, doc) {});

    res.json({ code: 200 });
  });
};
